#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBANKGUITEST ::..")

PROJECT(SKBBASEMODELERTEST)

ADD_DEFINITIONS(-DQT_GUI_LIB)
LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

INCLUDE_DIRECTORIES( ${CMAKE_SOURCE_DIR}/tests/skgbasemodelertest )

#Make executables
ADD_EXECUTABLE(skgtestpredicatcreator skgtestpredicatcreator.cpp)
ADD_EXECUTABLE(skgtestmainpanel skgtestmainpanel.cpp)
ADD_EXECUTABLE(skgtestactions skgtestactions.cpp)
ADD_EXECUTABLE(skgtesttreeview skgtesttreeview.cpp)
ADD_EXECUTABLE(skgtestbankwidgets skgtestbankwidgets.cpp)
ADD_EXECUTABLE(skgtestmodel skgtestmodel.cpp modeltest.cpp)


TARGET_LINK_LIBRARIES(skgtestpredicatcreator  Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Test skgbasemodeler skgbankmodeler skgbankgui skgbasegui)
TARGET_LINK_LIBRARIES(skgtestmainpanel  Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Test skgbasemodeler skgbankmodeler skgbankgui skgbasegui)
TARGET_LINK_LIBRARIES(skgtestactions  Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Test skgbasemodeler skgbankmodeler skgbankgui skgbasegui)
TARGET_LINK_LIBRARIES(skgtesttreeview Qt${QT_VERSION_MAJOR}::Core KF5::KIOWidgets Qt${QT_VERSION_MAJOR}::Test skgbankmodeler skgbasemodeler skgbankgui skgbasegui)
TARGET_LINK_LIBRARIES(skgtestbankwidgets Qt${QT_VERSION_MAJOR}::Core KF5::KIOWidgets Qt${QT_VERSION_MAJOR}::Test skgbankmodeler skgbasemodeler skgbankguidesigner skgbasegui)
TARGET_LINK_LIBRARIES(skgtestmodel Qt${QT_VERSION_MAJOR}::Core KF5::KIOWidgets Qt${QT_VERSION_MAJOR}::Test skgbankmodeler skgbasemodeler skgbankgui skgbasegui)

#Add test
ENABLE_TESTING()
ADD_TEST(NAME skgtestpredicatcreator COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtestpredicatcreator.sh)
ADD_TEST(NAME skgtestmainpanel COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtestmainpanel.sh)
ADD_TEST(NAME skgtestactions COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtestactions.sh)
ADD_TEST(NAME skgtesttreeview COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtesttreeview.sh)
ADD_TEST(NAME skgtestbankwidgets COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtestbankwidgets.sh)
ADD_TEST(NAME skgtestmodel COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtestmodel.sh)

#TODO: TO LONG ADD_TEST(NAME skgtestskrooge COMMAND ${CMAKE_SOURCE_DIR}/tests/scripts/skgtestskrooge.sh)

INCLUDE(CTest)
