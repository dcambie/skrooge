#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBASEMODELER ::..")

PROJECT(SKGBASEMODELER)
IF(SKG_DBUS)
    MESSAGE( STATUS "     DBUS enabled")
    ADD_DEFINITIONS(-DSKG_DBUS=${SKG_DBUS})
ELSE(SKG_DBUS)
    MESSAGE( STATUS "     DBUS disabled")
ENDIF(SKG_DBUS)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

FIND_PACKAGE(Qca-qt5 2.1.0 REQUIRED)                   
SET_PACKAGE_PROPERTIES(Qca-qt5 PROPERTIES DESCRIPTION "Support for encryption"
                       URL "https://download.kde.org/stable/qca-qt5"
                       TYPE REQUIRED)                       

INCLUDE_DIRECTORIES(${SQLCIPHER_INCLUDE_DIRS})
                       
IF ( Qca-qt5_FOUND )
   ADD_DEFINITIONS( -DQCA2 )
ENDIF()

SET(skgbasemodeler_SRCS
   skgobjectbase.cpp
   skgnamedobject.cpp
   skgnodeobject.cpp
   skgpropertyobject.cpp
   skgdocument.cpp
   skgdocumentprivate.cpp
   skgtransactionmng.cpp
   skgservices.cpp
   skgerror.cpp
   skgtraces.cpp
   skgadvice.cpp
   skgreport.cpp
   skgtreemap.cpp
 )

#build a shared library
ADD_LIBRARY(skgbasemodeler SHARED ${skgbasemodeler_SRCS})

#need to link to some other libraries ? just add them here
SET_TARGET_PROPERTIES(skgbasemodeler PROPERTIES VERSION ${SKG_VERSION} SOVERSION ${SOVERSION} )
TARGET_LINK_LIBRARIES(skgbasemodeler LINK_PUBLIC KF5::I18n KF5::IconThemes KF5::ConfigWidgets KF5::CoreAddons
    Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Sql Qt${QT_VERSION_MAJOR}::Script Qt${QT_VERSION_MAJOR}::Xml Qt${QT_VERSION_MAJOR}::DBus Qt${QT_VERSION_MAJOR}::Concurrent qca-qt5
    Grantlee5::Templates KF5::KIOCore
    ${SQLCIPHER_LIBRARIES} )

GENERATE_EXPORT_HEADER(skgbasemodeler BASE_NAME skgbasemodeler)

########### install files ###############
INSTALL(TARGETS skgbasemodeler ${KDE_INSTALL_TARGETS_DEFAULT_ARGS}  LIBRARY NAMELINK_SKIP )
